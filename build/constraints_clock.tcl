# Timing Analysis Assignments
# ===========================
set_global_assignment -name NUMBER_OF_PATHS_TO_REPORT 0
set_global_assignment -name NUMBER_OF_DESTINATION_TO_REPORT 0
set_global_assignment -name NUMBER_OF_SOURCES_PER_DESTINATION_TO_REPORT 0
#set_global_assignment -name MAX_SCC_SIZE 50

set_global_assignment -name OUTPUT_IO_TIMING_NEAR_END_VMEAS "HALF VCCIO" -rise
set_global_assignment -name OUTPUT_IO_TIMING_NEAR_END_VMEAS "HALF VCCIO" -fall
set_global_assignment -name OUTPUT_IO_TIMING_FAR_END_VMEAS "HALF SIGNAL SWING" -rise
set_global_assignment -name OUTPUT_IO_TIMING_FAR_END_VMEAS "HALF SIGNAL SWING" -fall

set_global_assignment -name SDC_FILE constraints_clock.sdc
