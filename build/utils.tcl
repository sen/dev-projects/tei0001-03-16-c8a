#-------------------------------------------------------------------------------
# Procedures:
#-------------------------------------------------------------------------------

proc rm_extension { filename } {
  foreach suffix { "\.bdf" "\.sv$" "\.v$" "\.vo$" "\.vhd$" "\.vqm$" } {
    regsub -all $suffix $filename "" fileroot
    set filename $fileroot
  }
  return $filename
}

proc index_src { liste_arg } {
	for { set i 1 } { $i < [llength $liste_arg] } { incr i } {
		set match [ regexp {\.tcl} [lindex $liste_arg $i]]
		#puts [format " [lindex $liste_arg $i] index %d match %d" $i $match]
		if { $match == 0 } {
				return $i }
		}
}

proc filetype { filename } {
  return [ \
    switch -regexp $filename {
      ^.*\.bdf$ { format "BDF_FILE"             }
      ^.*\.vhdl$ -
      ^.*\.vhd$ { format "VHDL_FILE"            }
      ^.*\.sv$  { format "SYSTEMVERILOG_FILE"   }
      ^.*\.v$   -
      ^.*\.vo$  { format "VERILOG_FILE"         }
      ^.*\.vqm$ { format "VQM_FILE"             }
      ^.*\.qip$ { format "QIP_FILE"             }
      ^.*\.vwf$ { format "VECTOR_WAVEFORM_FILE" }
      default   { format "VHDL_FILE"            }
    }
  ]
}
