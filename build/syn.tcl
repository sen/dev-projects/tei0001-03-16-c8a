# ---- Load Quartus II Tcl Project package
package require ::quartus::project
package require ::quartus::flow

source utils.tcl

# ---- Get files from env
  set hdl_files [split $env(SOURCE_FILES)]
# ---- Get top entity/module from env
  set top_entity $env(TOP_ENTITY)
# ---- Get constraint files from env
  set constraints_files [split $env(CONSTRAINT_FILES)]

# ---- Only open if not already open
  if { [project_exists $top_entity ] } {
  	project_open -cmp $top_entity $top_entity
      puts "---> Opening project $top_entity"
  } else {
  	project_new $top_entity
      puts "---> Creating project $top_entity"
  }

# ---- HDL Source Files
  foreach source_file $hdl_files {
    set filetype [ filetype $source_file ]
    set_global_assignment -name $filetype "$source_file"
    puts "---> Reading $source_file"
  }

# ---- Constraints files
  foreach cstr_file $constraints_files {
    source $cstr_file
  }
# ---- output directory (sof...)
  set out_dir $env(OUT_DIR)
  set_global_assignment -name PROJECT_OUTPUT_DIRECTORY $out_dir

# ---- Top level entity
  set_global_assignment -name "FOCUS_ENTITY_NAME" "$top_entity"

#---- Commit assignments
  export_assignments
  puts "---> Assignments done, starting compilation..."
# exit

#---- Compile using ::quartus::flow
  execute_flow -compile

