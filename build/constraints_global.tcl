# Device
   set_global_assignment -name FAMILY "MAX 10"
   set_global_assignment -name DEVICE 10M16SAU169C8G
   set_global_assignment -name DEVICE_FILTER_PACKAGE UFBGA
   set_global_assignment -name DEVICE_FILTER_PIN_COUNT 169
   set_global_assignment -name DEVICE_FILTER_SPEED_GRADE 8
   set_global_assignment -name MIN_CORE_JUNCTION_TEMP 0
   set_global_assignment -name MAX_CORE_JUNCTION_TEMP 85
   # default IO standard (so we dont have to secify it for all pins)
   # YES THE PARAMETER NAME IS STRATIX_DEVICE_IO_STANDARD even for a MAX10
   set_global_assignment -name STRATIX_DEVICE_IO_STANDARD "3.3-V LVTTL"
# Specific
   # global reset
   set_global_assignment -name ENABLE_DEVICE_WIDE_RESET ON
   # No EPCQ conf device on the board
   set_global_assignment -name ENABLE_CONFIGURATION_PINS OFF
   set_global_assignment -name USE_CONFIGURATION_DEVICE OFF
   set_global_assignment -name EXTERNAL_FLASH_FALLBACK_ADDRESS 00000000
   set_global_assignment -name ENABLE_OCT_DONE OFF
   # CRC
   set_global_assignment -name CRC_ERROR_OPEN_DRAIN OFF
   set_global_assignment -name ERROR_CHECK_FREQUENCY_DIVISOR 256
   # How do we use the internal flash
   set_global_assignment -name INTERNAL_FLASH_UPDATE_MODE "SINGLE IMAGE WITH ERAM"
# Compilation
   set_global_assignment -name EDA_DESIGN_ENTRY_SYNTHESIS_TOOL "<None>"
   set_global_assignment -name AUTO_ENABLE_SMART_COMPILE ON
   set_global_assignment -name SMART_RECOMPILE ON
   set_global_assignment -name NUM_PARALLEL_PROCESSORS ALL
   set_global_assignment -name CYCLONEII_RESERVE_NCEO_AFTER_CONFIGURATION "USE AS REGULAR IO"
   set_global_assignment -name POWER_PRESET_COOLING_SOLUTION "23 MM HEAT SINK WITH 200 LFPM AIRFLOW"
   set_global_assignment -name POWER_BOARD_THERMAL_MODEL "NONE (CONSERVATIVE)"
   set_global_assignment -name FLOW_ENABLE_POWER_ANALYZER ON
   set_global_assignment -name POWER_DEFAULT_INPUT_IO_TOGGLE_RATE "12.5 %"
# Message level
    set_global_assignment -name HDL_MESSAGE_LEVEL LEVEL1
# Disable power optimization to speed the compilation
   set_global_assignment -name OPTIMIZE_POWER_DURING_SYNTHESIS OFF
