# Utilization of the partitions
set_global_assignment -name INCREMENTAL_COMPILATION INCREMENTAL_SYNTHESIS
set_global_assignment -name INCREMENTAL_COMPILATION FULL_INCREMENTAL_COMPILATION

# Description of the partitions (example code)
set_global_assignment -name PARTITION_NETLIST_TYPE POST_FIT -section_id Top
set_global_assignment -name PARTITION_FITTER_PRESERVATION_LEVEL PLACEMENT_AND_ROUTING -section_id Top
set_instance_assignment -name PARTITION_HIERARCHY no_file_for_top_partition -to | -section_id Top
