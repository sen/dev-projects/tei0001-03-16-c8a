# main oscillator
set_location_assignment PIN_H6  -to clk_12mhz
# Optional oscillator
set_location_assignment PIN_G5 -to clk_x

# Buttons
set_location_assignment PIN_E7  -to reset_btn
set_location_assignment PIN_E6  -to user_btn
set_instance_assignment -name IO_STANDARD "3.3 V SCHMITT TRIGGER" -to user_btn
set_instance_assignment -name IO_STANDARD "3.3 V SCHMITT TRIGGER" -to reset_btn

set_location_assignment PIN_A8  -to led_out[0]
set_location_assignment PIN_A9  -to led_out[1]
set_location_assignment PIN_A11 -to led_out[2]
set_location_assignment PIN_A10 -to led_out[3]
set_location_assignment PIN_B10 -to led_out[4]
set_location_assignment PIN_C9  -to led_out[5]
set_location_assignment PIN_C10 -to led_out[6]
set_location_assignment PIN_D8  -to led_out[7]

set_location_assignment PIN_A4  -to uart_rxd
set_location_assignment PIN_B4  -to uart_txd
set_location_assignment PIN_B5  -to uart_cts
set_location_assignment PIN_A6  -to uart_rts
set_location_assignment PIN_B6  -to uart_dsr
set_location_assignment PIN_A7  -to uart_dtr

set_location_assignment PIN_A3  -to flash_spi_clk
set_location_assignment PIN_B3  -to flash_spi_csn
set_location_assignment PIN_A2  -to flash_spi_mosi
set_location_assignment PIN_B2  -to flash_spi_miso

set_location_assignment PIN_J5 -to g_sen_int1
set_location_assignment PIN_L5 -to g_sen_int2
set_location_assignment PIN_J6 -to g_sen_spi_clk
set_location_assignment PIN_L5 -to g_sen_spi_csn
set_location_assignment PIN_J7 -to g_sen_spi_mosi
set_location_assignment PIN_K5 -to g_sen_spi_miso

set_location_assignment PIN_K7  -to sdram_wen
set_location_assignment PIN_M7  -to sdram_rasn
set_location_assignment PIN_E9  -to sdram_dqm[0]
set_location_assignment PIN_F12 -to sdram_dqm[1]
set_location_assignment PIN_G10 -to sdram_dq[1]
set_location_assignment PIN_D11 -to sdram_dq[0]
set_location_assignment PIN_F10 -to sdram_dq[2]
set_location_assignment PIN_F9  -to sdram_dq[3]
set_location_assignment PIN_E10 -to sdram_dq[4]
set_location_assignment PIN_D9  -to sdram_dq[5]
set_location_assignment PIN_G9  -to sdram_dq[6]
set_location_assignment PIN_F8  -to sdram_dq[7]
set_location_assignment PIN_F13 -to sdram_dq[8]
set_location_assignment PIN_E12 -to sdram_dq[9]
set_location_assignment PIN_E13 -to sdram_dq[10]
set_location_assignment PIN_D12 -to sdram_dq[11]
set_location_assignment PIN_C12 -to sdram_dq[12]
set_location_assignment PIN_B12 -to sdram_dq[13]
set_location_assignment PIN_B13 -to sdram_dq[14]
set_location_assignment PIN_A12 -to sdram_dq[15]
set_location_assignment PIN_M4  -to sdram_csn
set_location_assignment PIN_M9  -to sdram_clk
set_location_assignment PIN_M8  -to sdram_cke
set_location_assignment PIN_N7  -to sdram_casn
set_location_assignment PIN_N6  -to sdram_ba[0]
set_location_assignment PIN_K8  -to sdram_ba[1]
set_location_assignment PIN_K6  -to sdram_addr[0]
set_location_assignment PIN_M5  -to sdram_addr[1]
set_location_assignment PIN_N5  -to sdram_addr[2]
set_location_assignment PIN_J8  -to sdram_addr[3]
set_location_assignment PIN_N10 -to sdram_addr[4]
set_location_assignment PIN_M11 -to sdram_addr[5]
set_location_assignment PIN_N9  -to sdram_addr[6]
set_location_assignment PIN_L10 -to sdram_addr[7]
set_location_assignment PIN_M13 -to sdram_addr[8]
set_location_assignment PIN_N8  -to sdram_addr[9]
set_location_assignment PIN_N4  -to sdram_addr[10]
set_location_assignment PIN_M10 -to sdram_addr[11]

# Headers

# PMOD
set_location_assignment PIN_M3  -to pio[0]
set_location_assignment PIN_L3  -to pio[1]
set_location_assignment PIN_M2  -to pio[2]
set_location_assignment PIN_M1  -to pio[3]
set_location_assignment PIN_N3  -to pio[4]
set_location_assignment PIN_N2  -to pio[5]
set_location_assignment PIN_K2  -to pio[6]
set_location_assignment PIN_K1  -to pio[7]

# Arduino MKR header
set_location_assignment PIN_H8   -to D[0]
set_location_assignment PIN_K10  -to D[1]
set_location_assignment PIN_H5   -to D[2]
set_location_assignment PIN_H4   -to D[3]
set_location_assignment PIN_J1   -to D[4]
set_location_assignment PIN_J2   -to D[5]
set_location_assignment PIN_L12  -to D[6]
set_location_assignment PIN_J12  -to D[7]
set_location_assignment PIN_J13  -to D[8]
set_location_assignment PIN_K11  -to D[9]
set_location_assignment PIN_K12  -to D[10]
#without pull-up resistors
set_location_assignment PIN_J10  -to D[11]
set_location_assignment PIN_H10  -to D[12]
#with pull-up resistors
#set_location_assignment PIN_D13 -to D[11]
#set_location_assignment PIN_G13 -to D[12]
set_location_assignment PIN_H13  -to D[13]
set_location_assignment PIN_G12  -to D[14]

set_location_assignment PIN_E1   -to AIN[0]
set_location_assignment PIN_C2   -to AIN[1]
set_location_assignment PIN_C1   -to AIN[2]
set_location_assignment PIN_D1   -to AIN[3]
set_location_assignment PIN_E3   -to AIN[4]
set_location_assignment PIN_E4   -to AIN[5]
set_location_assignment PIN_F1   -to AIN[6]
