module max1000 (
   clk_12mhz,
   reset_btn,
   user_btn,
   led_out,
   // FTDI uart
   uart_txd,
   uart_rxd,
   // Flash
   flash_spi_csn,
   flash_spi_clk,
   flash_spi_mosi,
   flash_spi_miso,
   // Accelerometer
   g_sen_spi_clk,
   g_sen_spi_csn,
   g_sen_spi_mosi,
   g_sen_spi_miso,
   // SDRAM
   sdram_clk,
   sdram_cke,
   sdram_csn,
   sdram_wen,
   sdram_rasn,
   sdram_casn,
   sdram_ba,
   sdram_addr,
   sdram_dqm,
   sdram_dq
);

input        clk_12mhz;
input        reset_btn;
input        user_btn;
output [7:0] led_out;
// FTDI uart
output       uart_txd;
input        uart_rxd;
// Flash
output       flash_spi_csn;
output       flash_spi_clk;
output       flash_spi_mosi;
input        flash_spi_miso;
// Accelerometer
output       g_sen_spi_clk;
output       g_sen_spi_csn;
output       g_sen_spi_mosi;
input        g_sen_spi_miso;
// SDRAM
output       sdram_clk;
output       sdram_cke;
output       sdram_csn;
output       sdram_wen;
output       sdram_rasn;
output       sdram_casn;
output [1:0] sdram_ba;
output [11:0]sdram_addr;
output [1:0] sdram_dqm;
inout  [15:0]sdram_dq;


logic[21:0] cpt;


always@(posedge clk_12mhz or negedge reset_btn)
   if(!reset_btn)
      cpt <= '0;
   else
      cpt <= cpt+1;

logic [7:0] sr;
always@(posedge clk_12mhz or negedge reset_btn)
   if(!reset_btn)
      sr <= 8'h1;
   else
      if(cpt == 0)
        sr <= {sr[6:0],sr[7]};

assign led_out = sr;


endmodule
